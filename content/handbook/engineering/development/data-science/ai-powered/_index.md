---
title: AI-Powered Stage
description: "The AI-Powered Stage in the Data Science section is focused on providing applied AI capabilties to the GitLab product."
---

## Vision

Build diverse and global development teams in the Data Science section to support GitLab's vision on the application of AI in the DevOps cycle,
while maintaining [our values](/handbook/values/) and [unique way of working](/handbook/company/culture/all-remote/guide/).

## Mission

Drive results through iterative development as we add AI features with flexible ML Models capabilities into the product.
Our teams are data-driven, support [dogfooding](https://about.gitlab.com/direction/dogfooding/), and [collaboration](/handbook/values/#collaboration) within GitLab and the wider community.

## Features

| Feature | Team Ownership | Project Location | Standalone or consideration for Chat Framework |
|---|---|---|---|
| [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions) | [Create:Code Creation Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-creation/) | [GitLab](https://gitlab.com/gitlab-org/gitlab), [GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension), [GitLab Web IDE](https://gitlab.com/gitlab-org/gitlab-web-ide), [GitLab JetBrains Plugin](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin), [GitLab Vim](https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim), [GitLab Visual Studio Extension](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension) | Standalone |
| [Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat) | [GitLab Duo Chat Group](https://handbook.gitlab.com/handbook/engineering/development/data-science/ai-powered/duo-chat/) | [GitLab](https://gitlab.com/gitlab-org/gitlab), [GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension), [GitLab Web IDE](https://gitlab.com/gitlab-org/gitlab-web-ide), [GitLab JetBrains Plugin](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin), [GitLab Vim](https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim), [GitLab Visual Studio Extension](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension) | Chat Framework |
| [Git suggestions](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli#gitlab-duo-commands) | [Create:Source Code](https://handbook.gitlab.com/handbook/engineering/development/dev/create/source-code-be/) | [CLI](https://gitlab.com/gitlab-org/cli#usage) | Standalone |
| [Discussion summary](https://docs.gitlab.com/ee/user/ai_features.html#summarize-issue-discussions-with-discussion-summary) | [Plan:Project Management Team](https://handbook.gitlab.com/handbook/engineering/development/dev/plan-project-management/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Chat Framework |
| [Issue description generation](https://docs.gitlab.com/ee/user/ai_features.html#summarize-an-issue-with-issue-description-generation) | [Plan:Project Management Team](https://handbook.gitlab.com/handbook/engineering/development/dev/plan-project-management/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Test generation](https://docs.gitlab.com/ee/user/gitlab_duo_chat#write-tests-in-the-ide) | [Create:Editor Extensions Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/) | [GitLab Web IDE](https://gitlab.com/gitlab-org/gitlab-web-ide) | Chat Framework |
| [Merge request template population](https://docs.gitlab.com/ee/user/project/merge_requests/ai_in_merge_requests#fill-in-merge-request-templates) | [Create:Code Review Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-review/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Suggested Reviewers](https://docs.gitlab.com/ee/user/project/merge_requests/reviews#gitlab-duo-suggested-reviewers) | [Create:Code Review Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-review/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Merge request summary](https://docs.gitlab.com/ee/user/project/merge_requests/ai_in_merge_requests#summarize-merge-request-changes) | [Create:Code Review Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-review/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Chat Framework |
| [Code review summary](https://docs.gitlab.com/ee/user/project/merge_requests/ai_in_merge_requests#summarize-my-merge-request-review) | [Create:Code Review Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/code-review/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Vulnerability summary](https://docs.gitlab.com/ee/user/application_security/vulnerabilities#explaining-a-vulnerability) | [Govern, Threat Insights](https://handbook.gitlab.com/handbook/engineering/development/sec/govern/threat-insights/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Vulnerability resolution](https://docs.gitlab.com/ee/user/application_security/vulnerabilities#vulnerability-resolution) | [Govern, Threat Insights](https://handbook.gitlab.com/handbook/engineering/development/sec/govern/threat-insights/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Code explanation](https://docs.gitlab.com/ee/user/ai_features.html#explain-code-in-the-web-ui-with-code-explanation) | [Create:Source Code](https://handbook.gitlab.com/handbook/engineering/development/dev/create/source-code-be/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Chat Framework |
| [Root cause analysis](https://docs.gitlab.com/ee/user/ai_features.html#root-cause-analysis) | [Verify:Pipeline Execution Group](https://handbook.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Value stream forecasting](https://docs.gitlab.com/ee/user/ai_features.html#forecast-deployment-frequency-with-value-stream-forecasting) | [Optimize Group](https://handbook.gitlab.com/handbook/engineering/development/dev/plan/optimize/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |
| [Product Analytics](https://docs.gitlab.com/ee/user/product_analytics/index.html) | [Product Analytics Group](https://handbook.gitlab.com/handbook/engineering/development/analytics/product-analytics/) | [GitLab](https://gitlab.com/gitlab-org/gitlab) | Standalone |

### AI Powered Operational Agreements

- **Sustainable way of working**: Getting all of our teams back to a sustainable way of working as a first priority. The ambiguity in work schedules, uncertainty around who has license to make decisions, and changes in priority lead to a lack of predictability for people's work, which creates stress and a reduced sense of psychological safety.
- **Collaboration**: Addressing collaboration problems thoroughly and in real time as they arise so we can reduce the their negative impact on business outcomes and encourage more psychological safety.
- **Roadmap review and revision**: Reducing the frequency of changes to near-term roadmap. Review our major roadmap items monthly, driven by a focus on accounting for market externalities, customer feedback, and rollout/scale pains.  We will approach major product milestone completions in a measured fashion so we can help teams be more efficient, plan implementations, and set expectations without being given a delivery timeline.
- **Communication cascade**: Improve our communication cascade to avoid reliance, build trust, provide additional context, align everyone on priorities, and prevent communication surprises.
- **Blocking work**: Address blocked work or critical responses being needed within a reasonable time to empower team members to drive progress and move forward.
- **Strategy for GitHub**: Communicate clearly our strategy for winning against GitHub so we can share a sense of urgency to compete and be a leader in the market.
- **Quad connection**: Ensure that all key team members of the quad are connected and informed (Infra, Quality, Dev, PM, UX) to expand transparency and collaboration as groups.
- **Communication cascade**: We have created 2 new Slack channels:
  - `ai-leads`: For senior leaders, EMs and PMs across the primary AI teams to make sure they have a place to align with each other.
  - `#ai-portfolio` to include the quads, engineers, designers, testers, infra, our key Marketing stakeholders, etc that are working on AI. This will help us provide a SSoT avenue for communication so we don’t confuse groups or disseminate information separately.

Other temporary channels and recurring meetings will be spun up as needed for individual AI projects.

- **Quad connection**: The AI leads have started making weekly team announcements for developments across the groups. These are found [here](https://gitlab.com/gitlab-org/ai-powered/ai-weekly/-/issues/?sort=created_date&state=all&label_name%5B%5D=AI%20Powered%20Weekly%20Updates&first_page_size=20).

### Business Continuity - Coverage and Escalation

The following table shows who will provide cover if one or more of the Data-Science Engineering management team are unable to work for any reason.

| Team Member        | Covered by            | Escalation     |
| -----              |-----------------------| -----          |
| Wayne Haber        | Bartek Marnane           | Jörg Heilig   |
| Phil Calder        | Jay Swain             | Wayne Haber    |
| Jay Swain          | Phil Calder           | Wayne Haber    |
| Monmayuri          | Bartek Marnane        | Jörg Heilig   |

If an issue arises  - such as a production incident or feature change lock - that a team member needs management support with when their direct manager is not online, the team member should reach out to any Data-Science Engineering Manager by mentioning in `#data-science-section`. The manager can help the team member follow the process and co-ordinate to ensure the team member has the necessary support.

If an Engineer is unavailable the Engineering Manager will reassign open issues and merge requests to another engineer, preferably in the same [group](#all-team-members).

Some people management functions may require escalation or delegation, such as [Workday](/handbook/people-group/workday/workday-guide/) and [Navan Expense](/handbook/business-technology/enterprise-applications/guides/navan-expense-guide/).

This can be used as the basis for a business continuity plan (BCP),
as well as a general guide to Data Science Engineering continuity in the event of one or more team members being unavailable for any reason.

## Stage Groups
